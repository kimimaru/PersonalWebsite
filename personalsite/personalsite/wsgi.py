"""
WSGI config for personalsite project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os, sys

# These lines are required so WSGI knows where the packages for your site are 
sys.path.append('/var/www/personalsite')
sys.path.append('/var/www/personalsite/personalsite')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'personalsite.settings')

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
