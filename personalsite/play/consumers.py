import json
from asgiref.sync import async_to_sync
from channels.generic.websocket import AsyncWebsocketConsumer

class InputConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = "play" #self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = self.room_name #'play_%s' % self.room_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        if not text_data:
            return

        print(text_data)

        text_data_json = None
            
        try:
            text_data_json = json.loads(text_data)
            print("Found JSON!")
        except: 
            print("Invalid JSON. Maybe a handshake?")

            if not text_data_json:
                text_data_json = json.dumps({
                    'User': None,
                    'Message': {
                        'Text': text_data
                    }
                })

            print(text_data_json)

            # Send message to room group
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': text_data_json
                }
            )

                #message = text_data_json['message']

                # self.send(text_data=json.dumps({
                #     'user': None,
                #     'message': {
                #         'Text': message
                #     }
                # }))
    
    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=message)
        
        # json.dumps({
        #     'message': message
        # }))