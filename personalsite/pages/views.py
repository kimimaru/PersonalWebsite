from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.views import generic
from django.urls import reverse

def IndexView(request):
    return render(request, "pages/index.html")

def AboutView(request):
    return render(request, "pages/about.html")

def FreelancingView(request):
    return render(request, "pages/freelancing.html")

def ProjectsView(request):
    return render(request, "pages/projects/projects.html")

def ProjectPageView(request, project_name):
    return render(request, "pages/projects/" + str(project_name) + ".html")

def Show404View(request, exception):
    return render(request, "pages/404.html")

def Show500View(request):
    return render(request, "pages/404.html")