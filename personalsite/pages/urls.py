from django.urls import path

from . import views

app_name = "pages"
urlpatterns = [
    path("", views.IndexView, name="index"),
    path("about", views.AboutView, name="about"),
    path("freelancing", views.FreelancingView, name="freelancing"),
    path("projects", views.ProjectsView, name="projects"),
    path("projects/<str:project_name>", views.ProjectPageView, name="project"),
]