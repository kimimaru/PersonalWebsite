/* MIT License

Copyright © 2020 Kimimaru

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/* Notes  
- Don't expect this to work in your project out of the box, as there's some code specific to Maze Burrow in here. The code was
  left in as an example of a few ways to optimize this
- EnumUtility.HasEnumVal performs a simple bitwise check without generating garbage from enums. It's equivalent to: (flag1 & flag2) != 0
*/

/// <summary>
/// The flags for recording objects.
/// <para>This is a bitwise field.</para>
/// </summary>
[Flags]
public enum RecordableFlags
{
    /// <summary>
    /// No special properties - the current value will be recorded.
    /// </summary>
    None = 0,

    /// <summary>
    /// A clone of the value will be recorded.
    /// </summary>
    Clone = 1 << 0,

    /// <summary>
    /// The fields and properties of the value will be recorded.
    /// </summary>
    RecordFields = 1 << 1,

    /// <summary>
    /// The value will not be set to the field and property of the object.
    /// This is a minor performance gain for reference types that are not cloned but have their fields and properties recorded.
    /// </summary>
    AvoidSet = 1 << 2,

    /// <summary>
    /// The value will be added to the main object list that's processed.
    /// </summary>
    AddToList = 1 << 3,

    /// <summary>
    /// The value will not be recorded in the data of the object it belongs to.
    /// </summary>
    DontRecordInBelongingObject = 1 << 4
}

/// <summary>
/// An attribute used to indicate a recordable field or property on an object.
/// <para>For recordable events, use the explicit syntax.
/// It's possible the backing field has the same name as the event, but it's likely not guaranteed
/// and increases the complexity of finding the backing field.</para>
/// </summary>
[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
public sealed class RecordableAttribute : Attribute
{
    /// <summary>
    /// The recordable flags of this attribute.
    /// </summary>
    public RecordableFlags RecordedFlags { get; private set; } = RecordableFlags.None;

    public RecordableAttribute(RecordableFlags recordableFlags)
    {
        RecordedFlags = recordableFlags;
    }
}

/// <summary>
/// Contains utilities for recording object data to undo moves.
/// </summary>
public static class RecordingGlobals
{
    /// <summary>
    /// The BindingFlags for obtaining recorded object members.
    /// </summary>
    public const BindingFlags RecordBindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

    /// <summary>
    /// The max number of moves that are recorded before erasing older entries.
    /// </summary>
    public const int MaxMoveRecords = 30;

    /// <summary>
    /// The cached MemberwiseClone method invoked on objects that must be cloned.
    /// </summary>
    private static MethodInfo MemberwiseCloneMethod = null;

    static RecordingGlobals()
    {
        //Cache the memberwise clone method
        MemberwiseCloneMethod = typeof(object).GetMethod(nameof(MemberwiseClone), RecordBindingFlags);
    }

    #region Recording and Undoing

    public sealed class RecordedLevelData
    {
        public List<ICollisionObj> CollisionList = null;
        public List<RecordedObjData> ObjData = null;
    }

    public sealed class RecordedObjData
    {
        private RecordedLevelData RecLevelData = null;
        public object Obj { get; private set; } = null;
        public Transform ObjTransform { get; private set; } = null;

        public Vector2 Position { get; private set; }
        public float Rotation { get; private set; }
        public Vector2 Scale { get; private set; }

        /// <summary>
        /// The data held for each object.
        /// The string is the name of the field or property,
        /// and the value is either an object or a <see cref="RecordedObjData"/> if that object is recorded.
        /// </summary>
        private Dictionary<string, object> Data = null;

        public RecordedObjData(object obj, RecordedLevelData recLevelData)
        {
            Obj = obj;
            RecLevelData = recLevelData;
        }

        public void RecordObject()
        {
            //Record individual transform information if this is a transformable object
            if (Obj is ITransformable transformable)
            {
                ObjTransform = transformable.transform;

                Position = ObjTransform.Position;
                Rotation = ObjTransform.Rotation;
                Scale = ObjTransform.Scale;
            }

            Type objType = Obj.GetType();

            //Get all fields and properties of the object
            FieldInfo[] fields = GetFieldsForType(objType);
            PropertyInfo[] properties = GetPropertiesForType(objType);

            int fieldsLength = fields.Length;
            int propertiesLength = properties.Length;

            int dataCount = fieldsLength + propertiesLength;

            //If no fields or properties are present in this type, return
            if (dataCount == 0)
            {
                return;
            }

            //Initialize with enough capacity to fit all fields and properties
            Data = new Dictionary<string, object>(dataCount);

            //Use the Length property of the array to eliminate bounds checking in this loop for a minor performance gain
            for (int i = 0; i < fields.Length; i++)
            {
                FieldInfo field = fields[i];

                RecordableFlags recFlags = GetRecordableFlagsForMember(field);

                object val = field.GetValue(Obj);
                if (val != null)
                {
                    if (EnumUtility.HasEnumVal((long)recFlags, (long)RecordableFlags.Clone) == true)
                    {
                        val = MemberwiseCloneMethod.Invoke(val, null);
                    }
                }

                RecordedObjData recordedVal = null;

                if (val != null)
                {
                    if (EnumUtility.HasEnumVal((long)recFlags, (long)RecordableFlags.RecordFields) == true)
                    {
                        if (recordedVal == null)
                        {
                            recordedVal = new RecordedObjData(val, RecLevelData);
                        }
                        recordedVal.RecordObject();
                    }

                    if (EnumUtility.HasEnumVal((long)recFlags, (long)RecordableFlags.AddToList) == true)
                    {
                        if (recordedVal == null)
                        {
                            recordedVal = new RecordedObjData(val, RecLevelData);
                        }
                        RecLevelData.ObjData.Add(recordedVal);
                    }

                    if (EnumUtility.HasEnumVal((long)recFlags, (long)RecordableFlags.DontRecordInBelongingObject) == true)
                    {
                        continue;
                    }
                }

                if (recordedVal != null)
                {
                    Data.Add(field.Name, recordedVal);
                }
                else
                {
                    Data.Add(field.Name, val);
                }
            }

            //Use the Length property of the array to eliminate bounds checking in this loop for a minor performance gain
            for (int i = 0; i < properties.Length; i++)
            {
                PropertyInfo property = properties[i];

                RecordableFlags recFlags = GetRecordableFlagsForMember(property);
                
                object val = property.GetValue(Obj);
                if (val != null)
                {
                    if (EnumUtility.HasEnumVal((long)recFlags, (long)RecordableFlags.Clone) == true)
                    {
                        val = MemberwiseCloneMethod.Invoke(val, null);
                    }
                }

                RecordedObjData recordedVal = null;

                if (val != null)
                {
                    if (EnumUtility.HasEnumVal((long)recFlags, (long)RecordableFlags.RecordFields) == true)
                    {
                        if (recordedVal == null)
                        {
                            recordedVal = new RecordedObjData(val, RecLevelData);
                        }
                        recordedVal.RecordObject();
                    }

                    if (EnumUtility.HasEnumVal((long)recFlags, (long)RecordableFlags.AddToList) == true)
                    {
                        if (recordedVal == null)
                        {
                            recordedVal = new RecordedObjData(val, RecLevelData);
                        }
                        RecLevelData.ObjData.Add(recordedVal);
                    }

                    if (EnumUtility.HasEnumVal((long)recFlags, (long)RecordableFlags.DontRecordInBelongingObject) == true)
                    {
                        continue;
                    }
                }

                if (recordedVal != null)
                {
                    Data.Add(property.Name, recordedVal);
                }
                else
                {
                    Data.Add(property.Name, val);
                }
            }
        }

        public void SetObjectProperties()
        {
            if (ObjTransform != null)
            {
                ObjTransform.Position = Position;
                ObjTransform.Rotation = Rotation;
                ObjTransform.Scale = Scale;
            }

            foreach (KeyValuePair<string, object> dat in Data)
            {
                object objData = dat.Value;

                Type objType = Obj.GetType();

                MemberInfo member = GetMemberForTypeAndName(objType, dat.Key);

                if (member.MemberType == MemberTypes.Field)
                {
                    FieldInfo field = (FieldInfo)member;

                    RecordableFlags recFlags = GetRecordableFlagsForMember(field);
                    
                    RecordedObjData recObjData = objData as RecordedObjData;

                    if (EnumUtility.HasEnumVal((long)recFlags, (long)RecordableFlags.AvoidSet) == false)
                    {
                        if (recObjData != null)
                        {
                            field.SetValue(Obj, recObjData.Obj);
                        }
                        else
                        {
                            field.SetValue(Obj, objData);
                        }
                    }

                    if (recObjData != null && recObjData.Data != null)
                    {
                        recObjData.SetObjectProperties();
                    }
                }
                else if (member.MemberType == MemberTypes.Property)
                {
                    PropertyInfo property = (PropertyInfo)member;

                    //Look through the object hierarchy in the event of a private setter
                    if (property.GetSetMethod(true) == null)
                    {
                        Type baseObjType = objType.BaseType;
                        while (baseObjType != null && baseObjType != typeof(object))
                        {
                            PropertyInfo prop = baseObjType.GetProperty(property.Name, RecordBindingFlags);

                            if (prop != null && prop.GetSetMethod(true) != null)
                            {
                                property = prop;
                                break;
                            }

                            baseObjType = baseObjType.BaseType;
                        }
                    }

                    RecordableFlags recFlags = GetRecordableFlagsForMember(property);

                    RecordedObjData recObjData = objData as RecordedObjData;

                    if (EnumUtility.HasEnumVal((long)recFlags, (long)RecordableFlags.AvoidSet) == false)
                    {
                        if (recObjData != null)
                        {
                            property.SetValue(Obj, recObjData.Obj);
                        }
                        else
                        {
                            property.SetValue(Obj, objData);
                        }
                    }

                    if (recObjData != null && recObjData.Data != null)
                    {
                        recObjData.SetObjectProperties();
                    }
                }
            }
        }
    }

    #endregion

    #region Reflection Caches

    /// <summary>
    /// The cache for recordable flags.
    /// </summary>
    private static readonly Dictionary<MemberInfo, RecordableFlags> RecordableCache = new Dictionary<MemberInfo, RecordableFlags>(512);

    /// <summary>
    /// Obtains a <see cref="RecordableFlags"/> value for a member that has a <see cref="RecordableAttribute"/>.
    /// </summary>
    /// <param name="member">The MemberInfo to get the <see cref="RecordableFlags"/> value for.</param>
    /// <returns>The <see cref="RecordableFlags"/> that the member has on an existing <see cref="RecordableAttribute"/>.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static RecordableFlags GetRecordableFlagsForMember(MemberInfo member) => RecordableCache[member];

    /// <summary>
    /// The cache for each Type's fields.
    /// </summary>
    private static readonly Dictionary<Type, FieldInfo[]> TypeFields = new Dictionary<Type, FieldInfo[]>(256);

    /// <summary>
    /// The cache for each Type's properties.
    /// </summary>
    private static readonly Dictionary<Type, PropertyInfo[]> TypeProperties = new Dictionary<Type, PropertyInfo[]>(256);

    /// <summary>
    /// Obtains an array of fields for a given Type and caches it.
    /// </summary>
    /// <param name="type">The Type to obtain the fields for.</param>
    /// <returns>An array of fields for the given type.</returns>
    private static FieldInfo[] GetFieldsForType(Type type)
    {
        if (TypeFields.TryGetValue(type, out FieldInfo[] fields) == false)
        {
            fields = type.GetFields(RecordBindingFlags);
            fields = CondenseFieldArray(fields);
            TypeFields.Add(type, fields);
        }

        return fields;
    }

    /// <summary>
    /// Obtains an array of properties for a given Type and caches it.
    /// </summary>
    /// <param name="type">The Type to obtain the properties for.</param>
    /// <returns>An array of properties for the given type.</returns>
    private static PropertyInfo[] GetPropertiesForType(Type type)
    {
        if (TypeProperties.TryGetValue(type, out PropertyInfo[] properties) == false)
        {
            properties = type.GetProperties(RecordBindingFlags);
            properties = CondensePropertyArray(type, properties);
            TypeProperties.Add(type, properties);
        }

        return properties;
    }

    /// <summary>
    /// Condenses an array of FieldInfo to exclude fields without a <see cref="RecordableAttribute"/>.
    /// Those that do contain a <see cref="RecordableAttribute"/> cache the <see cref="RecordableFlags"/> for the field.
    /// </summary>
    /// <param name="fields">An array of FieldInfo.</param>
    /// <returns>A shortened array of FieldInfo containing only fields that have a <see cref="RecordableAttribute"/>.</returns>
    private static FieldInfo[] CondenseFieldArray(FieldInfo[] fields)
    {
        if (fields.Length == 0) return Array.Empty<FieldInfo>();

        //Wrap in a list and remove for all non-recordable fields
        List<FieldInfo> wrappedList = new List<FieldInfo>(fields);

        for (int i = wrappedList.Count - 1; i >= 0; i--)
        {
            FieldInfo field = wrappedList[i];

            //Check for a recordable attribute
            RecordableAttribute recordable = field.GetCustomAttribute<RecordableAttribute>();

            //Remove since one doesn't exist
            if (recordable == null)
            {
                wrappedList.RemoveAt(i);
            }
            else
            {
                //Add to our cache
                RecordableCache.Add(field, recordable.RecordedFlags);
            }
        }

        return wrappedList.ToArray();
    }

    /// <summary>
    /// Condenses an array of PropertyInfo to exclude properties without a <see cref="RecordableAttribute"/>.
    /// Those that do contain a <see cref="RecordableAttribute"/> cache the <see cref="RecordableFlags"/> for the property.
    /// </summary>
    /// <param name="properties">An array of PropertyInfo.</param>
    /// <returns>A shortened array of PropertyInfo containing only properties that have a <see cref="RecordableAttribute"/>.</returns>
    private static PropertyInfo[] CondensePropertyArray(Type objType, PropertyInfo[] properties)
    {
        if (properties.Length == 0) return Array.Empty<PropertyInfo>();

        //Wrap in a list and remove for all non-recordable properties
        List<PropertyInfo> wrappedList = new List<PropertyInfo>(properties);

        for (int i = wrappedList.Count - 1; i >= 0; i--)
        {
            PropertyInfo property = wrappedList[i];

            //Check for a recordable attribute
            RecordableAttribute recordable = property.GetCustomAttribute<RecordableAttribute>();

            if (recordable == null)
            {
                wrappedList.RemoveAt(i);
                continue;
            }

            //For properties, make sure they have a get and set method
            if (property.GetGetMethod(true) == null)
            {
                wrappedList.RemoveAt(i);
                continue;
            }

            bool found = false;

            //Look through the object hierarchy in the event of a private setter
            if (property.GetSetMethod(true) == null)
            {
                Type baseObjType = objType;
                while (baseObjType != null && baseObjType != typeof(object))
                {
                    PropertyInfo prop = baseObjType.GetProperty(property.Name, RecordBindingFlags);

                    if (prop != null && prop.GetSetMethod(true) != null)
                    {
                        property = prop;
                        found = true;
                        break;
                    }

                    baseObjType = baseObjType.BaseType;
                }
            }
            else
            {
                found = true;
            }

            if (found == false)
            {
                wrappedList.RemoveAt(i);
                continue;
            }

            //Overwrite the property here in the event the one with a setter is in a base class
            wrappedList[i] = property;
            
            //Add to our cache if it's not here
            if (RecordableCache.ContainsKey(property) == false)
                RecordableCache.Add(property, recordable.RecordedFlags);
        }

        return wrappedList.ToArray();
    }

    /// <summary>
    /// The cache for the members of a given name associated with each Type.
    /// </summary>
    private static Dictionary<Type, Dictionary<string, MemberInfo>> TypeMembers = new Dictionary<Type, Dictionary<string, MemberInfo>>(256);

    /// <summary>
    /// Obtains an array of members for a given type and member name.
    /// </summary>
    /// <param name="type">The Type to get the members for.</param>
    /// <param name="memberName">The name of the member.</param>
    /// <returns>An array of members with the given <paramref name="memberName"/> that <paramref name="type"/> has.</returns>
    private static MemberInfo GetMemberForTypeAndName(Type type, string memberName)
    {
        if (TypeMembers.TryGetValue(type, out Dictionary<string, MemberInfo> memberDict) == false)
        {
            memberDict = new Dictionary<string, MemberInfo>(32);

            TypeMembers.Add(type, memberDict);
        }
        
        if (memberDict.TryGetValue(memberName, out MemberInfo member) == false)
        {
            MemberInfo[] members = type.GetMember(memberName, MemberTypes.Field | MemberTypes.Property, RecordBindingFlags);
            member = members[0];

            memberDict.Add(memberName, member);
        }

        return member;
    }

    #endregion
}